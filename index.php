<?php
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "https://newsapi.org/v2/everything?domains=wsj.com&apiKey=cf3dc09cd01d4c2ea1f6640b31ae5985");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($curl);
    curl_close($curl);

    $data = json_decode($output, true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>News</title>
    <link href="style.css" rel="stylesheet">
    <link href="css/responsive/responsive.css" rel="stylesheet">
</head>
<body>
    <header class="header_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-expand-lg">
                        <div class="collapse navbar-collapse justify-content-center" id="news-nav">
                            <ul class="navbar-nav" id="news-nav">
                              <li class="nav-item active"><a class="nav-link" href="index.php">News</a></li>
                              <li class="nav-item"><a class="nav-link" href="bussines.php">Bussines</a></li>
                              <li class="nav-item"><a class="nav-link" href="science.php">Sci-Tech</a></li>
                              <li class="nav-item"><a class="nav-link" href="health.php">Health</a></li>
                              <li class="nav-item"><a class="nav-link" href="entertaiment.php">Entertaiment</a></li>
                              <li class="nav-item"><a class="nav-link" href="otomotif.php">Otomotif</a></li>
                              <li class="nav-item"><a class="nav-link" href="sports.php">Sports</a></li>
                              <li class="nav-item"><a class="nav-link" href="carrer.php">Carrer</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <section class="blog_area section_padding_0_80">
        <div class="container">
            <div class="row">
            <?php foreach($data['articles'] as $d){ ?>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-post wow fadeInUp" data-wow-delay="0.1s">
                        <div class="post-thumb">
                            <img src="<?php echo $d['urlToImage']; ?>">
                        </div>
                            <h4 class="post-headline"><a><?php echo $d['title']; ?></a></h4>
                            <a href="<?php echo $d['url']; ?>" class="primary-btn">Read more</a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>

    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <script src="js/bootstrap/popper.min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <script src="js/others/plugins.js"></script>
    <script src="js/active.js"></script>
</body>
